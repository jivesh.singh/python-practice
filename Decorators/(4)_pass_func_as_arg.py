# Passing function as an argument

def hello():
    return 'Hello World'

def other(some_func):
    print('Other code would go here')
    print(some_func())


print(hello())              # Hello World

print('\n')

print(hello)                # <function hello at 0x00F97778>

print('\n')


other(hello)

# Output
"""
Other code would go here
Hello World
"""