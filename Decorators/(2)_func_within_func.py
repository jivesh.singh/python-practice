# Implementing functions within a function

def hello(name='Jose'):
    print('The hello() function has been executed')
    
    def greet():
        return '\t This is inside the greet() function'
    
    def welcome():
        return "\t This is inside the welcome() function"
    
    print(greet())
    print(welcome())
    print("Now we are back inside the hello() function")



hello()


# Output
'''
The hello() function has been executed
	 This is inside the greet() function
	 This is inside the welcome() function
Now we are back inside the hello() function
'''


print("\n")


# Due to scope, the welcome() function cannot be accessed outside of the hello() function

try:
    welcome()
except NameError:
    print("NameError: name 'welcome' is not defined")