# Assigning function to a variable

"""
Remember that in Python everything is an object. That means functions are objects which can be assigned labels and passed into other functions.
"""


def hello():
    return("Hello World")


print(hello())     # Hello World



# Assigning hello() to 'greet' variable

"""
Assign another label to the function. Note that we are not using parentheses here because we are not calling the function hello, instead we are just passing a function object to the greet variable.
"""


greet= hello

print(greet())          # Hello World



# Deleting hello()

del hello

try:
    hello()
except:
    print("Cannot access hello() as it has been deleted")




print(greet())      # Hello World



"""
Even though we deleted the name hello, the name greet still points to our original function object and performs its functionality. It is important to know that functions are objects that can be passed to other objects!
"""