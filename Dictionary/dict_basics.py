# Define a blank dictionary with no elements
blank_dict = {}

# Define a dictionary with numeric keys
num_dict = {1: 'soccer', 2: 'baseball'}

# Define a dictionary with keys having different types
misc_dict = {'class': 'senior secondary', 1: [1, 2, 3,4,5]}

# Create a dictionary with dict() method
get_dict_from_func = dict({1:'veg', 2:'non-veg'})

# Create a dictionary from a sequence of tuples
make_dict_from_seq = dict([(1,'jake'), (2,'john')])


# dict() Constructor

print("\n========== dict() Constructor ==========\n")

thisdict = dict(brand="Ford", model="Mustang", year=1964)
# note that keywords are not string literals
# note the use of equals rather than colon for the assignment

print(thisdict)         # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964}



# Adding elements to dictionary (without method)

print("\n========== Add elements without method ==========\n")

dic= {'k1':1,'k2':2}

print("Before: ",dic)           # {'k1': 1, 'k2': 2}

dic['k3']= 3

print("After: ",dic)            # {'k1': 1, 'k2': 2, 'k3': 3}




# Removing Elements using del keyword

print("\n========== del Keyword ==========\n")

thisdict = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

print("Before Deletion: ", thisdict)        # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964}

del thisdict["model"]

print("After Deletion: ", thisdict)         # {'brand': 'Ford', 'year': 1964}


print("\nDeleting dictionary\n")

del thisdict

try:
    print(thisdict)

except Exception as ex:
    print("\nError: ", ex)




# Membership Test

print("\n========== Membership Test ==========\n")

weekdays = {'fri': 5, 'tue': 2, 'wed': 3, 'sat': 6, 'thu': 4, 'mon': 1, 'sun': 0}

print('fri' in weekdays)            # Output: True

print('thu' not in weekdays)        # Output: False

print('mon' in weekdays)            # Output: True