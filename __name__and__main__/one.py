# If we run the script one.py directly, then the value of built-in variable __name__ will be '__main__'
# There always an if condition at last line of code to check whether __name__ == '__main__'
# This is to check whether the file is being imported or run directly.


def func():
    print("FUNC() in ONE.PY")


print("TOP level in ONE.PY ==> 0th indentation")



if __name__ == '__main__':

    # This means if the script is running directly.

    print("ONE.PY is being run directly.")

else:
    print("ONE.PY has been imported.")