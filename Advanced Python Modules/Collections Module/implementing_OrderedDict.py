"""
An OrderedDict is a dictionary subclass that remembers the order in which its contents are added.
"""

# Normal Dictionary

"""
Dictionaries are unordered.

From Python 3.6 onwards, dictionaries are insertion ordered. This means that the standard dict type maintains insertion order by default, but there are no indices.
"""

d= {}

d['a'] = 1
d['b'] = 2
d['c'] = 3
d['d'] = 4

for (k,v) in d.items():
    print(k, v)


# Output
"""
a 1
b 2
c 3
d 4
"""



print("\n")



# Ordered Dictionary

from collections import OrderedDict

od= OrderedDict()

od['a'] = 1
od['b'] = 2
od['c'] = 3
od['d'] = 4

for (k,v) in od.items():
    print(k, v)

# Output
"""
a 1
b 2
c 3
d 4
"""



print("\n")



# Equality
"""
A regular dict looks at its contents when testing for equality.
An OrderedDict considers the order in which the items were added.
"""

# Equality in normal dictionary
d1 = {}
d1['a'] = 'A'
d1['b'] = 'B'

d2 = {}
d2['b'] = 'B'
d2['a'] = 'A'

print(d1==d2)               # True




# Equality in OrderedDict
d1 = OrderedDict()
d1['a'] = 'A'
d1['b'] = 'B'


d2 = OrderedDict()

d2['b'] = 'B'
d2['a'] = 'A'

print(d1==d2)               # False