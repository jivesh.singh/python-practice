"""
defaultdict is a dictionary-like object which provides all methods provided by a dictionary but takes a first argument (default_factory) as a default data type for the dictionary.

Using defaultdict is faster than doing the same using dict.set_default method.

"""


# Importing Library
from collections import defaultdict


"""
A defaultdict will never raise a KeyError.
"""

# Using regular dictionary

d= {'k1':1}

try:
    print(d['k2'])
except KeyError:
    print("KeyError: 'k2' is not defined.")



print("\n")


# Using defaultdict

"""
Any key that does not exist gets the value returned as whatever the default factory condition of the first argument is.
"""

d= defaultdict(object)
print(d['k5'])                  # <object object at 0x000865C8>

dic1= defaultdict(list)
print(dic1['one'])              # []

dic2= defaultdict(set)
print(dic2['one'])              # set()

dic3= defaultdict(str)
print(dic3['new_key'])          # Empty String
print(type(dic3['new_key']))    # <class 'str'>

dic4= defaultdict(tuple)
print(dic4['some_key'])         # ()

dic5= defaultdict(dict)
print(dic5['some_key'])         # {}




""" We can also initialise the default values """

# Initialising default value as 0 if key does not exist
some_dic = defaultdict(lambda: 0)

print(some_dic['one'])          # 0

some_dic['two']= 2

print(some_dic.items())         # dict_items([('one', 0), ('two', 2)])