"""
Counter is a dict subclass which helps count hashable objects.

Inside of it, elements are stored as dictionary keys and the counts of the objects are stored as the value.
"""

# Importing Library
from collections import Counter


# Counter() with lists
lst= [1,1,2,2,2,3,4,4,5]

print(Counter(lst))             # Counter({2: 3, 1: 2, 4: 2, 3: 1, 5: 1})


# Counter() with strings
s= 'Waba lubba dub dub'

print(Counter(s))               # Counter({'b': 5, 'a': 3, ' ': 3, 'u': 3, 'd': 2, 'W': 1, 'l': 1})




# Count words in a sentence
sentence= "How many words times the words are appearing times"

words= sentence.split()

print(Counter(words))            

# Counter({'words': 2, 'times': 2, 'How': 1, 'many': 1, 'the': 1, 'are': 1, 'appearing': 1})







# Counter() methods

print("\n========== Counter Methods ==========\n")

s = 'Waba lubba dub dub'

c = Counter(s)

# Finding Two Most Common Words
print(c.most_common(2))                     # [('b', 5), ('a', 3)]


"""

Common methods when using the Counter() object :-

sum(c.values())                 # total of all counts

c.clear()                       # reset all counts

list(c)                         # list unique elements

set(c)                          # convert to a set

dict(c)                         # convert to a regular dictionary

c.items()                       # convert to a list of (elem, cnt) pairs

Counter(dict(list_of_pairs))    # convert from a list of (elem, cnt) pairs

c.most_common()[:-n-1:-1]       # n least common elements

c += Counter()                  # remove zero and negative counts

"""