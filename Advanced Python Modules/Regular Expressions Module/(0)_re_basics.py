"""
One of the most common uses for the re module is for finding patterns in text.
"""

import re

# Example 1
# Getting Match object

print(re.search('hello','hello world'))

# <re.Match object; span=(0, 5), match='hello'>

print(re.search('h','w'))           # None


print("\n")


# Example 2
patterns=['term1','term2']

text= 'This is a string with term1, but not the other term'


for pattern in patterns:
    print('Searching for "%s" in:\n "%s"\n' %(pattern,text))
    
    #Check for match
    if re.search(pattern,text):
        print('Match was found. \n')
    else:
        print('No Match was found.\n')



# Output
"""
Searching for "term1" in:
 "This is a string with term1, but not the other term"

Match was found.

Searching for "term2" in:
 "This is a string with term1, but not the other term"

No Match was found.
"""



"""
The Match object returned by the search() method is more than just a Boolean or None. 

It contains information about the match, including the original input string, the regular expression that was used, and the location of the match.
"""

match = re.search('hello','hello world')

print(type(match))          # <class 're.Match'>

# Show start index of the match
print(match.start())        # 0

# Show the end index of match
print(match.end())          # 5



print("\n")


# Finding all the instances of a match

"""
You can use re.findall() to find all the instances of a pattern in a string.
"""

result= re.findall('match','Here is one match, here is another match')

print(result)       # ['match', 'match']