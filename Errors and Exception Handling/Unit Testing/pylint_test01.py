""" Implementing pylint testing """

def my_func():

    """
    Sample function to test pylint
    """

    first = 10
    second = 12

    print(first)
    print(second)


my_func()

# This code is rated 10/10
