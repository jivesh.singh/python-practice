"""
There are dozens of good testing libraries out there. Most are third-party packages that require an install, such as:

pylint
pyflakes
pep8


However, these are simple tools that merely look at your code, and they'll tell you if there are style issues or simple problems like variable names being called before assignment.

A far better way to test your code is to write tests that send sample data to your program, and compare what's returned to a desired outcome.

Two such tools are available from the standard library:

unittest
doctest
"""


# Using pylint

# Pylint is a Python static code analysis tool which looks for programming errors, helps enforcing a coding standard, sniffs for code smells and offers simple refactoring suggestions.

# For Python 3.6+ versions, upgrade pylint using command 'pip install pylint --upgrade'

# Execute the following code in cmd using the command 'pylint unit_testing.py'.

# Make sure the python file is in the same location

a = 100
b = 2

print(a)
print(b)


# Output
"""
************* Module unit_testing_basics
unit_testing_basics.py:2:0: C0301: Line too long (117/100) (line-too-long)
unit_testing_basics.py:9:0: C0301: Line too long (180/100) (line-too-long)
unit_testing_basics.py:11:0: C0301: Line too long (141/100) (line-too-long)
unit_testing_basics.py:22:0: C0301: Line too long (183/100) (line-too-long)
unit_testing_basics.py:41:0: C0301: Line too long (104/100) (line-too-long)
unit_testing_basics.py:42:0: C0301: Line too long (104/100) (line-too-long)
unit_testing_basics.py:47:0: C0304: Final newline missing (missing-final-newline)
unit_testing_basics.py:28:0: C0103: Constant name "a" doesn't conform to UPPER_CASE naming style (invalid-name)
unit_testing_basics.py:29:0: C0103: Constant name "b" doesn't conform to UPPER_CASE naming style (invalid-name)
unit_testing_basics.py:36:0: W0105: String statement has no effect (pointless-string-statement)

----------------------------------------------------------------------
Your code has been rated at -10.00/10 (previous run: 10.00/10, -20.00)
"""







# Using unittest

"""
'unittest' supports test automation, sharing of setup and shutdown code for tests, aggregation of tests into collections, and independence of the tests from the reporting framework.

The unittest module provides classes that make it easy to support these qualities for a set of tests.

To achieve this, unittest supports the following important concepts −

    1. test fixture − This represents the preparation needed to perform one or more tests, and any associate cleanup actions. This may involve, for example, creating temporary or proxy databases, directories, or starting a server process.

    2. test case − This is the smallest unit of testing. This checks for a specific response to a particular set of inputs. unittest provides a base class, TestCase, which may be used to create new test cases.

    3. test suite − This is a collection of test cases, test suites, or both. This is used to aggregate tests that should be executed together. Test suites are implemented by the TestSuite class.

    4. test runner − This is a component which orchestrates the execution of tests and provides the outcome to the user. The runner may use a graphical interface, a textual interface, or return a special value to indicate the results of executing the tests.



The following steps are involved in writing a simple unit test −

Step 1 − Import the unittest module in your program.

Step 2 − Define a function to be tested.

Step 3 − Create a testcase class by inheriting unittest.TestCase.

Step 4 − Define a test as a method inside the class. Name of method must start with 'test'.

Step 5 − Each test calls assert function of TestCase class. There are many types of asserts.

Step 6 − assertEquals(arg1, arg2) function compares result of tested function (arg1) with expected output (arg2) and throws assertionError if comparison fails.

Step 7 − Finally, call main() method from the unittest module.




The following three could be the possible outcomes of a test −
	
1. OK

- The test passes. ‘A’ is displayed on console.

2. FAIL

- The test does not pass, and raises an AssertionError exception. ‘F’ is displayed on console.

3. ERROR

- The test raises an exception other than AssertionError. ‘E’ is displayed on console.
"""