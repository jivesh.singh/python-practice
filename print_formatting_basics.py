# Using Placeholders

# The symbol '%' is the string formatting operator.

print("\n========== Using Placeholder ==========\n")

print("I'm going to inject %s here." %'something')
# Output: I'm going to inject something here.

print('\n')

print("I'm going to inject %s text here, and %s text here." %('some','more'))
# Output: I'm going to inject some text here, and more text here.

print("\n")

x, y = 'some', 'more'
print("I'm going to inject %s text here, and %s text here."%(x,y))
# Output: I'm going to inject some text here, and more text here.

print("\n")

# Format conversion in placeholder

# 1.
# %s and %r convert any python object to a string using two separate methods: str() and repr().

# Note that %r and repr() deliver the string representation of the object, including quotation marks and any escape characters.

print('He said his name was %s.' %'Fred')
print('He said his name was %r.' %'Fred')

# Output
"""
He said his name was Fred.
He said his name was 'Fred'.
"""

print("\n")

print('I once caught a fish %s.' %'this \tbig')
print('I once caught a fish %r.' %'this \tbig')

# Output
"""
I once caught a fish this       big.
I once caught a fish 'this \tbig'.
"""

print("\n")

#2.
# %s and %d.

# The %s operator converts whatever it sees into a string, including integers and floats. The %d operator converts numbers to integers first, without rounding. 

print('I wrote %s programs today.' %3.75)
print('I wrote %d programs today.' %3.75)  

# Output
"""
I wrote 3.75 programs today.
I wrote 3 programs today.
"""

print("\n")



# Padding and Precision of floating point no.s in placeholder.
"""
Floating point numbers use the format %5.2f. Here, 5 would be the minimum number of characters the string should contain; these may be padded with whitespace if the entire number does not have this many digits. Next to this, .2f stands for how many numbers to show past the decimal point. 
"""

print('Floating point numbers: %5.2f' %(13.144))
# Floating point numbers: 13.14

print("\n")

print('Floating point numbers: %1.0f' %(13.144))
# Floating point numbers: 13

print("\n")

print('Floating point numbers: %1.5f' %(13.144))
# Floating point numbers: 13.14400

print("\n")

print('Floating point numbers: %10.2f' %(13.144))
# Floating point numbers:      13.14

print("\n")

print('Floating point numbers: %25.2f' %(13.144))
# Floating point numbers:                     13.14

print("\n")


# Multiple Formatting

print('First: %s, Second: %5.2f, Third: %r' %('hi!',3.1415,'bye!'))
# First: hi!, Second:  3.14, Third: 'bye!'

print("\n")




# Using .format method

print("\n========== Using .format method ==========\n")

print("This string is {}.".format('INSERTED'))
# This string is INSERTED.

print("\n")

print("The {} {} {}.".format('fox','brown','quick'))
# The fox brown quick.

print("\n")


# Using Indexing in .format

print("The {2} {1} {0}.".format('fox','brown','quick'))
# The quick brown fox.

print("\n")

print("The {0} {0} {0}.".format("fox","brown","quick"))
# The fox fox fox.

print("\n")


# Assigning Keywords in .format method

print("The {q} {b} {f}.".format(f="fox",b="brown",q="quick"))
# The quick brown fox.

print("\n")



# Float formatting using .format method

# {value:width.precision f}

# value -> Passed value
# width -> length of characters
# precision -> limit of decimal places

result= 100/77
print("The result is {r}".format(r=result))
# The result is 1.2987012987012987

print("\n")

print("The result is {r:1.3f}".format(r=result))        # Precision upto 3 decimal places
# The result is 1.299

print("\n")

print("The result is {r:10.3f}".format(r=result))       
# Width of 10 characters. Whitespaces are padded to fill the length.
# The result is      1.299

print("\n")







# Using formatted string literals (f-strings)

print("\n========== f-strings ==========\n")

name= "Jivesh"
print(f"My name is {name}.")
# My name is Jivesh.

print("\n")


# Float formatting using f-strings

# {value:{width}.{precision}}

res= 100/88
print(res)          # 1.1363636363636365

print(f"Result is: {res:{10}.{5}}")
# Result is:     1.1364