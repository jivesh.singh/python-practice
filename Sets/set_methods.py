# Set Methods

print("\n========== Set Methods ==========\n")




# 1. copy() -> Returns a copy of the set

print("\n***** Copy *****\n")

py_set= {1,2,3,"hello"}

new_set= py_set.copy()

print(new_set)                  # {1, 2, 3, 'hello'}




# 2. isdisjoint() ->  Returns True if none of the items are present in both sets, otherwise it returns False.

print("\n***** isdisjoint *****\n")

x= {1,2,3,4}
y= {'a','b','c'}

z= x.isdisjoint(y)      # Return True if no items in set x is present in set y
print(z)                # True




# 3. issubset() -> The issubset() method returns True if all elements of a set are present in another set (passed as an argument). If not, it returns False.

print("\n***** issubset *****\n")

A= {1,2,3,4,5}
B= {3,4}

print(A.issubset(B))        # False
print(B.issubset(A))        # True




# 4. issuperset() -> The issuperset() method returns True if all items in the specified set exists in the original set, otherwise it retuns False.

print("\n***** issuperset() *****\n")

A= {1,2,3,4,5}
B= {3,4}

print(A.issuperset(B))      # True
print(B.issuperset(A))      # False




print("\n========== Adding elements ==========\n")

print("\n***** Add *****\n")

# 5. add() -> Adds an element to a set. 

py_set= {1,2,3,4}

py_set.add("hello")
print(py_set)           # {1, 2, 3, 4, 'hello'}

py_set.add(99)
print(py_set)           # {1, 2, 3, 4, 99, 'hello'}


# If the element already exists, the add() method does not add the element.

py_set.add(2)
print(py_set)            # {1, 2, 3, 4, 99, 'hello'}




print("\n***** Update *****\n")

# 6. update() -> It updates the current set, by adding items from another set.

x= {1,2,3}
print(x)        # {1, 2, 3}

y= {4,5,6}

x.update(y)
print(x)        # {1, 2, 3, 4, 5, 6}

print("\n")

# If an item is present in both sets, only one appearance of this item will be present in the updated set.

x= {'a','e','i','o','u'}
print(x)            # {'e', 'a', 'u', 'i', 'o'}

y= {'a','b','c','d','e'}

x.update(y)
print(x)            # {'e', 'd', 'b', 'c', 'a', 'u', 'i', 'o'}

print("\n")

# The update() method can even accept tuples, lists, strings, or other sets as an argument.

py_set_num = {77, 88}

py_set_num.update([4.4, 5.5, 6.6], {2.2, 4.4, 6.6})

print(py_set_num)       # {2.2, 4.4, 5.5, 6.6, 77, 88}




print("\n========== Remove Elements ==========\n")

print("\n***** remove() *****\n")

# 7. remove() -> It removes the specified element. 
#             -> It will throw the “KeyError” error if the specified element is not present in the set.

fruits = {"apple", "banana", "cherry"}
print("Before Removing: ", fruits)              # {'cherry', 'apple', 'banana'}

fruits.remove("banana")
print("After Removing: ",fruits)                # {'cherry', 'apple'}


# remove an element not present in the set

py_set_num = {22, 33, 55, 77, 99}

try:
    py_set_num.remove(44)
except Exception as ex:
    print("py_set_num.remove(44) => KeyError:", ex)



print("\n***** discard() *****\n")

# 8. discard() -> It removes the specified element. 
#             -> It will not throw any error if the specified element is not present in the set.


fruits = {"apple", "banana", "cherry"}
print("Before discarding: ", fruits)    #  {'cherry', 'banana', 'apple'}

fruits.discard("banana")
print("After discarding: ", fruits)     # {'cherry', 'apple'}


# discard an element not present in the set

py_set_num = {22, 33, 55, 77, 99}

py_set_num.discard(44)
print("py_set_num.discard(44):", py_set_num)    # py_set_num.discard(44): {33, 99, 77, 22, 55}



print("\n***** pop() *****\n")

# 9. pop() -> It removes a random item from the set because of no indexing.
#          -> This method returns the removed item.

fruits = {"apple", "banana", "cherry", "mango"}

print(fruits.pop())     # apple
print(fruits)           # {'cherry', 'banana', 'mango'}

print(fruits.pop())     # cherry
print(fruits)           # {'banana', 'mango'}



print("\n***** clear() *****\n")

# 10. clear() -> Removes all the elements from the set.

fruits = {"apple", "banana", "cherry"}
fruits.clear()

print(fruits)   # set()




print("\n========== Set Operations ==========\n")

print("\n***** Union *****\n")

# 11. union() -> It returns a set that contains all items from the original set, and all items from the specified sets.

# You can specify as many sets you want, separated by commas.

# If an item is present in more than one set, the result will contain only one appearance of this item.


x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

z = x.union(y)

print(z)            # {'cherry', 'microsoft', 'apple', 'banana', 'google'}


# Unify more than 2 sets

x = {"a", "b", "c"}
y = {"f", "d", "a"}
z = {"c", "d", "e"}

result = x.union(y, z)

print(result)           # {'a', 'd', 'e', 'b', 'f', 'c'}


# We can also perform union operation using the "|" operator.

set_a= {1,2,3,4,5}
set_b= {3,8,1,5,2}

print(set_a | set_b)    # {1, 2, 3, 4, 5, 8}



print("\n***** Intersection *****\n")

# 12. intersection() -> It returns a set that contains the similarity between two or more sets.

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

z = x.intersection(y)                   

print(z)                                # {'apple'}


# Comparing more than 2 sets.

x = {"a", "b", "c"}
y = {"c", "d", "e"}
z = {"f", "g", "c"}

result = x.intersection(y, z)

print(result)                           # {'c'}


# We can also perform intersection operation using the '&' operator.

set_a= {1,2,3,4,5}
set_b= {5,6,7,8}

print(set_a & set_b)        # {5}



print("\n***** Intersection Update *****\n")

# 13. intersection_update() -> Removes the items in this set that are not present in other, specified set(s)

"""
The intersection_update() method is different from the intersection() method, because the intersection() method returns a new set, without the unwanted items, and the intersection_update() method removes the unwanted items from the original set.
"""

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

x.intersection_update(y)

print(x)                        # {'apple'}


# Comparing more than 2 sets

x = {"a", "b", "c"}
y = {"c", "d", "e"}
z = {"f", "g", "c"}

x.intersection_update(y, z)

print(x)                        # {'c'}




print("\n***** Difference *****\n")

# 14. difference() -> Returns a set containing difference of two or more sets.
#                   -> The returned set contains items that exist only in the first set, and not in both sets.

# Example 1
x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

z = x.difference(y)

print(z)                    # {'banana', 'cherry'}

# Example 2
x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

z = y.difference(x)

print(z)                    # {'google', 'microsoft'}


# We can perform difference operation using the '-' operator

set_a= {1,2,3,4,5}
set_b= {3,5,8,1,9}

diff_ab= set_a - set_b
print(diff_ab)              # {2, 4}

diff_ba= set_b - set_a
print(diff_ba)              # {8, 9}



print("\n***** Difference Update *****\n")

# 15. difference_update() -> It removes the items that exist in both sets

"""
The difference_update() method is different from the difference() method, because the difference() method returns a new set, without the unwanted items, and the difference_update() method removes the unwanted items from the original set.
"""

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

x.difference_update(y)

print(x)                    # {'cherry', 'banana'}



print("\n****** Symmetric Difference ******\n")

# 16. symmetric_difference() -> It returns a set that contains items not present in the specified set(s).

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

z = x.symmetric_difference(y)

print(z)                            # {'banana', 'microsoft', 'google', 'cherry'}


# We can perform the symmetric_difference operation using the caret (^) operator.

set_a= {1,2,3,4,5}
set_b= {5,7,1,6,9,2}

symdiff_ab= set_a ^ set_b
print(symdiff_ab)               # {3, 4, 6, 7, 9}

symdiff_ba= set_b ^ set_a
print(symdiff_ba)                  # {3, 4, 6, 7, 9}



print("\n***** Symmetric Difference Update *****\n")

# 17. symmetric_difference_update() -> It updates the original set by removing items that are present in both sets, and inserting the other items.

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

x.symmetric_difference_update(y)

print(x)                            # {'google', 'microsoft', 'banana', 'cherry'}