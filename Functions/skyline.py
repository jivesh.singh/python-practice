# Define a function called myfunc  that takes in a string, and returns a matching string where every even letter is uppercase, and every odd letter is lowercase.

# Assume that the incoming string only contains letters, and don't worry about numbers, spaces or punctuation. 

# The output string can start with either an uppercase or lowercase letter, so long as letters alternate throughout the string.

# Eg: myfunc("Anthropomorphism") ----> aNtHrOpOmOrPhIsM




def myfunc(text):
    
    result= ""
    
    for i in range(len(text)):
        # For even letters
        if (i+1) % 2 == 0:
            result+= text[i].upper()
        
        # For odd letters
        else:
            result+= text[i].lower()
    
    return result




print(myfunc("Python"))     # pYtHoN