# Check whether a given no. is prime or not

def check_prime(num):

    prime= True

    # Check for 0 or 1
    if num<2:
        prime= False

    # Check for 2
    elif num==2:
        pass

    # Check for greater than 2
    else:
        # Check for prime
        if num%2 == 0:
            prime= False

        else:

            # Step of 2 to avoid even no.s as they are not prime
            for i in range(3,num,2):
                
                if num%i==0:
                    prime= False
                    break
                else:
                    continue

    return prime








# Write a function that returns the number of prime numbers that exist up to and including a given number
# count_primes(100) --> 25

def count_primes(num):
    
    # Check for 0 or 1 input
    if num<2:
        return 0
    
    # 2 or greater
    
    # Store our prime no.s
    primes= [2]
    
    # Counter
    x = 3
    
    # x goes through every number upto num
    while x<=num:
        # Check if x is prime
        for y in primes:
            if x%y == 0:
                x+=2
                break
                
        else:
            primes.append(x)
            x+=2
            
            
    print(f"List of prime no.s: {primes}")
    
    return len(primes)
                











print("\n========== Check a no. for prime ==========\n")

print(check_prime(3))       # True

print(check_prime(4))       # False

print(check_prime(17))      # True

print(check_prime(2))       # True

print(check_prime(407))     # False

print(check_prime(15))      # True







print("\n========== Give no. of prime no.s upto the given number ==========\n")

print(count_primes(100))

print("\n")

print(count_primes(10))