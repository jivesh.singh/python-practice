#1 Numbers

num1= 123
num2= 12.3
num3= 12 + 6j

print(type(num1))   # <class 'int'>
print(type(num2))   # <class 'float'>
print(type(num3))   # <class 'complex'>

# ===============================================

#2 String

str1= 'This string is within single quotes'
str2= "This string is within double quotes"
str3= """This multiline string is 
enclosed in 
triple quotes"""

print(str1+"\n"+str2+"\n"+str3)

print(type(str1),type(str2),type(str3),sep="\n")

# Output
# This string is within single quotes
# This string is within double quotes
# This multiline string is 
# enclosed in 
# triple quotes
# <class 'str'>
# <class 'str'>
# <class 'str'>

# ==================================================


#3 List

lst= [1,2,3,4,5,"Jivesh",1,22,[1,2,'a',1.345],{1,2}]
print(type(lst))

# Output
# <class 'list'>


# ===================================================


#4 Tuple

tup= (1,2,3,['a','b'],'c',{'k1':10,'k2':20},1.22,20.087)
print(type(tup))

# Output
# <class 'tuple'>


# =====================================================

#5 Dictionary

dic= {'key1': 12, 'key2': 'a', 'key3': [1,2,3], 'key4': ('a','b','c'), 5: 'value5'}
print(type(dic))

# Output
# <class 'dict'>

# =====================================================

#6 Sets

set1= {1,1,3,4,5,'a','a'}
print(set1)
print(type(set1))

# Output
# {1, 3, 4, 5, 'a'}
# <class 'set'>

# =======================================================

#7 Boolean

b1= True
b2= False

print(type(b1),type(b2),sep="\n")

# =======================================================

#8 Binary types

byt= b"Hello"
bytarr= bytearray(5)
memview= memoryview(bytes(5))

print(byt)
print(type(byt))

print(bytarr)
print(type(bytarr))

print(memview)
print(type(memview))

# Output
# b'Hello'
# <class 'bytes'>
# bytearray(b'\x00\x00\x00\x00\x00')
# <class 'bytearray'>
# <memory at 0x7f9c792571c8>
# <class 'memoryview'>