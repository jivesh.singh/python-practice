# Modify/Update Tuple
# Once a tuple is created, you cannot change its values. This is because tuples are immutable.

print("\n========== Modify/Update Tuple ==========\n")

py_tuple = (22, 33, 55, 66, [88, 99])
print("Tuple before modificaton:", py_tuple)

print("\n")

# Let's try to modify py_tuple
# It'll return a TypeError
try:
    py_tuple[0] = 11
except Exception as ex:
    print("OP(py_tuple[0]) Error:", ex)

print("\n")

# We can change the values of mutable
# elements inside the py_tuple i.e. list
py_tuple[4][0] = 77
py_tuple[4][1] = 88
print("Tuple after modificaton:", py_tuple)

print("\n")

# We can assign a tuple with new data
py_tuple = ('mon', 'tue', 'wed')
print("Tuple after reassignment:", py_tuple)

print("\n")


#  You can convert the tuple into a list, change the list, and convert the list back into a tuple

x = ("apple", "banana", "cherry")

print("Tuple x before modification: ", x)

y = list(x)
y[1] = "kiwi"
x = tuple(y)

print("\nTuple x after modification: ", x)

print("\n")



# Output

"""
Tuple before modificaton: (22, 33, 55, 66, [88, 99])

OP(py_tuple[0]) Error: 'tuple' object does not support item assignment

Tuple after modificaton: (22, 33, 55, 66, [77, 88])

Tuple after reassignment: ('mon', 'tue', 'wed')


Tuple x before modification:  ('apple', 'banana', 'cherry')

Tuple x after modification:  ('apple', 'kiwi', 'cherry')
"""




# Concatenation (+)

print("\n========== Concatenation (+) ==========\n")

first_tuple = ('p', 'y', 't')
second_tuple = ('h', 'o', 'n')
full_tuple = first_tuple + second_tuple

print(full_tuple)               # ('p', 'y', 't', 'h', 'o', 'n')




# Repetition (*)

print("\n========== Repetition (*) ==========\n")

init_tuple = ("fork", )
fork_tuple = init_tuple * 5

print(fork_tuple)               # ('fork', 'fork', 'fork', 'fork', 'fork')




print("\n*******************************************************************\n")




# Remove/Delete a tuple

print("\n========== Remove/Delete a tuple ==========\n")

# Tuples are immutable
# So you cannot remove items from it, but you can delete the tuple completely.

py_tuple = ('p', 'y', 't', 'h', 'o', 'n')


# You can't delete a particular item from a tuple
try:
    del py_tuple[0]
except Exception as ex:
    print("OP(del py_tuple[0]) Error:", ex)


print("\n")


# But you can delete a whole tuple
del py_tuple

try:
    print(py_tuple)
except Exception as ex:
    print("print(py_tuple) => Error:", ex)
    print("\nTuple deleted successfully!!")


# Output

"""
OP(del py_tuple[0]) Error: 'tuple' object doesn't support item deletion

print(py_tuple) => Error: name 'py_tuple' is not defined

Tuple deleted successfully!!
"""




print("\n************************************************************\n")




# Membership test in Tuples

print("\n========== Membership test in tuples ==========\n")

thistuple = ("apple", "banana", "cherry")

print('apple' in thistuple)                     # True

print('orange' in thistuple)                    # False






# Access Tuple Elements (Tuple Unpacking)

print("\n========== Tuple Unpacking ==========\n")

lst= [(2,4),(6,8),(10,12)]

# Without Unpacking
for tup in lst:
    print(tup)

# Output
"""
(2, 4)
(6, 8)
(10, 12)
"""


print("\n")


# With Unpacking
for (t1,t2) in lst:
    print(t1)

# Output
"""
2
6
10
"""